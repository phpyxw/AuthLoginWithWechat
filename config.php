<?php

return [
    [
        //小程序秘钥
        'name'    => 'appId',
        //配置标题
        'title'   => '小程序appId',
        //配置类型,支持string/text/number/datetime/array/select/selects/image/images/file/files/checkbox/radio/bool
        'type'    => 'string',
        //配置值
        'value'   => '',
        'content' =>
            [],
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => '',
    ],[
        //小程序秘钥
        'name'    => 'secretId',
        //配置标题
        'title'   => '小程序秘钥',
        //配置类型,支持string/text/number/datetime/array/select/selects/image/images/file/files/checkbox/radio/bool
        'type'    => 'string',
        //配置值
        'value'   => '',
        'content' =>
            [],
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => '',
    ],
    [
        //获取openid接口
        'name'    => 'loginUrl',
        //配置标题
        'title'   => '获取openid接口',
        //配置类型,支持string/text/number/datetime/array/select/selects/image/images/file/files/checkbox/radio/bool
        'type'    => 'string',
        //配置值
        'value'   => 'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code',
        'content' =>
            [],
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => '',
    ],
    [
        //获取access_token接口
        'name'    => 'accessUrl',
        //配置标题
        'title'   => '获取access_token接口',
        //配置类型,支持string/text/number/datetime/array/select/selects/image/images/file/files/checkbox/radio/bool
        'type'    => 'string',
        //配置值
        'value'   => 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s',
        'content' =>
            [],
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => '',
    ],
    [
        'name'    => '__tips__',
        'title'   => '温馨提示',
        'type'    => 'string',
        'content' =>
            [],
        'value'   => '1.别的配置可以按需删除，配置文件路径在addons/baseconfig/config.php 里面 <br>
                      1.通过 get_addon_config("baseconfig") 拿到所有配置数据 <br>
                      3.插件下面的确定保存按钮没有显示出来，没关系，在application/admin/view/addon/config.html 文件里面101行 class 为 form-group layer-footer 的div 里面 加上 style="display: block!important"  即可，重点是div显示 display: block',
        'rule'    => '',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => '',
    ]
];
