ALTER TABLE `__PREFIX__user`
    ADD COLUMN `country`  varchar(50) NULL COMMENT '国家-微信小程序' AFTER `verification`,
    ADD COLUMN `province`  varchar(50) NULL COMMENT '省会-微信小程序' AFTER `country`,
    ADD COLUMN `city`  varchar(50) NULL COMMENT '城市-微信小程序' AFTER `province`,
    ADD COLUMN `openid`  varchar(50) NULL COMMENT 'openid-微信小程序' AFTER `city`,
    ADD COLUMN `unionid`  varchar(50) NULL COMMENT 'unionid-微信小程序' AFTER `openid`,
    DROP INDEX `username`,
    DROP INDEX `email`;






